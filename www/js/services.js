angular.module('askCharlieApp.services', [])

// Interface related services
.factory('InterfaceService', function($rootScope, $window, $ionicLoading, $ionicPopup){
  
  return {

    // Loading state control
    showLoading: function(){
      $ionicLoading.show({template: 'Warten Sie mal...'});
    },

    hideLoading: function(){
      $ionicLoading.hide();
    },

    // Error messages
    showErrorDialog: function(message){
      $ionicPopup.alert({
       title: 'Fehler',
       template: message
      });
    },

    showGenericErrorDialog: function(){
      this.showErrorDialog('Ein Fehler ist aufgetreten.');
    },

    // Open some url in native browser
    openInBrowser: function(url){
      $window.open(url, '_system'); 
    }

  }

})


//
// Temporary fake services
//

// Services related to login / authentication
.factory('AuthenticationService', function($q, $timeout, InterfaceService) {

  return {
    // Login service
    login: function(loginData){
        InterfaceService.showLoading();
        return $q(function(resolve, reject){
          $timeout(function(){
            InterfaceService.hideLoading();
            resolve();
          }, 500);
        });
    }
  };

})

// Services related to content services
.factory('ContentService', function($q, $http, InterfaceService) {
  var baseUrl = '/';

  // Workaround to get local json data. It will make non sense if we use a real webservice
  if(ionic.Platform.isWebView()){
    if(ionic.Platform.isIOS()){
      baseUrl = '';
    }else if(ionic.Platform.isAndroid()){
      baseUrl = '/android_asset/www/'
    }
  }

  return {
    // Get categories of askCharlie
    getAskCharlieCategories: function(){
        InterfaceService.showLoading();
        return $q(function(resolve, reject){
          $http({
              method: 'GET',
              url: baseUrl + 'fake-data/askCharlie-categories.json'
          }).success(function(data) {
              InterfaceService.hideLoading();
              resolve(data);
          }).error(function(error) {
              InterfaceService.hideLoading();
              reject();
          });
        });
    },

    // Get categories of sign up
    getSignUpCategories: function(){
        InterfaceService.showLoading();
        return $q(function(resolve, reject){
          $http({
              method: 'GET',
              url: baseUrl + 'fake-data/signUp-categories.json'
          }).success(function(data) {
              InterfaceService.hideLoading();
              resolve(data);
          }).error(function(error) {
              InterfaceService.hideLoading();
              reject();
          });
        });
    }
  };

})

// Services related to contact
.factory('ContactService', function($q, $timeout, InterfaceService){

  return {
    // Sent contact to server
    send: function(contactData) {
        InterfaceService.showLoading();
        return $q(function(resolve, reject){
          $timeout(function(){
            InterfaceService.hideLoading();
            resolve();
          }, 500);
        });
    }
  };

});