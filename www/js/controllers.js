angular.module('askCharlieApp.controllers', ['askCharlieApp.services'])

.controller('AppCtrl', function($scope, $rootScope, $window, $ionicHistory, $ionicLoading, $state, $ionicModal, $timeout, AuthenticationService, InterfaceService) {

  // If it is the first access we show summary
  if(!$window.localStorage["askCharlieAlreadyOpened"]){
    $window.localStorage.setItem("askCharlieAlreadyOpened", true);
    $state.go('summary');
  }


  //
  // Global variables
  //

  // Login control state
  $rootScope.userLoggedIn = false;

  //
  // Login modal
  //

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.loginModal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.loginModal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.loginModal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    if($scope.loginForm.$valid){
      AuthenticationService.login($scope.loginData).then(function(){
        $rootScope.userLoggedIn = true;
        $scope.closeLogin();
      }, function(){
        InterfaceService.showGenericErrorDialog();
      });
    }
  };


  //
  // Logout modal
  //

  // Create the logout modal that we will use later
  $ionicModal.fromTemplateUrl('templates/logout.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.logoutModal = modal;
  });

  // Triggered in the logout modal to close it
  $scope.closeLogout = function() {
    $scope.logoutModal.hide();
  };

  // Open the logout modal
  $scope.logout = function() {
    $scope.logoutModal.show();
    $rootScope.goToHome();
  };

  // Perform the logout
  $scope.doLogout = function() {
    $scope.closeLogout();
    $rootScope.userLoggedIn = false;
  };

  //
  // Global functions
  //

  // Global function to go to home without history
  $rootScope.goToHome = function(){
    $ionicHistory.nextViewOptions({
        historyRoot: true
    });
    $state.go('app.home');
  };

  // Global function to open signup page in a browser
  $rootScope.openSignUp = function(){
    InterfaceService.openInBrowser('https://experts.askcharlie.de/signup/category');
  };

})

.controller('HomeCtrl', function($scope, $rootScope, $state, $ionicHistory, $ionicPopup, ContentService, InterfaceService) {
  
  $scope.categories = [];
  $scope.askCharlieData = {};

  // Get categories
  ContentService.getAskCharlieCategories().then(function(data){
    $scope.categories = data;
  }, function(){
    InterfaceService.showGenericErrorDialog();
  });


  // Open questionnaire in a browser
  $scope.goToQuestionnaire = function(askCharlieForm){
    if(askCharlieForm.$valid){
      InterfaceService.openInBrowser('https://projects.askcharlie.de/questionnaire/' + $scope.askCharlieData.category + '?zip=' + $scope.askCharlieData.zipCode);
    }
  }

})

.controller('HowItWorksCtrl', function($scope, $ionicSideMenuDelegate, $ionicScrollDelegate) {
  
  // Disable menu swip in this page
  $scope.$on('$ionicView.enter', function(){
      $ionicSideMenuDelegate.canDragContent(false);
  });

  $scope.$on('$ionicView.leave', function(){
      $ionicSideMenuDelegate.canDragContent(true);
  });

  // Scroll Top if change the slide
  $scope.slideHasChanged = function($index){
    $ionicScrollDelegate.scrollTop();
  };

})

.controller('AreYouAnExpertCtrl', function($scope, $state, $ionicSideMenuDelegate, $ionicScrollDelegate, ContentService, InterfaceService) {
  
  // Disable menu swip in this page
  $scope.$on('$ionicView.enter', function(){
      $ionicSideMenuDelegate.canDragContent(false);
  });

  $scope.$on('$ionicView.leave', function(){
      $ionicSideMenuDelegate.canDragContent(true);
  });

  // Scroll Top if change the slide
  $scope.slideHasChanged = function($index){
    $ionicScrollDelegate.scrollTop();
  };

  // Categories to bind select element
  $scope.categories = [];

  // Get categories
  ContentService.getSignUpCategories().then(function(data){
    $scope.categories = data;
  }, function(){
    InterfaceService.showGenericErrorDialog();
  });

})

.controller('ContactCrtl', function($scope, $rootScope, ContactService, InterfaceService) {
  
  // Contact data used to send to server
  $scope.contactData = {};

  // Send message to server
  $scope.sendMessage = function(contactForm){
    if(contactForm.$valid){
      // Send contact data to server
      ContactService.send($scope.contactData).then(function(){
        $scope.messageSent = true;
      }, function(data){
        InterfaceService.showGenericErrorDialog();
      });
    }
  };

});

