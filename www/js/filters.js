angular.module('askCharlieApp.filters', [])

// Filter to make some url be trusted
.filter('toTrusted', ['$sce', function($sce) {
    return function(text) {
        return $sce.trustAsResourceUrl(text);
    };
}]);