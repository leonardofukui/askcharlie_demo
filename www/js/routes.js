angular.module('askCharlieApp.routes', [])

// Route 
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('summary', {
    url: '/summary',
    templateUrl: 'templates/summary.html'
  })

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.home', {
      url: '/home',
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html',
          controller: 'HomeCtrl'
        }
      }
  })

  .state('app.how-it-works', {
    url: '/how-it-works',
    views: {
      'menuContent': {
        templateUrl: 'templates/how-it-works.html',
        controller: 'HowItWorksCtrl'
      }
    }
  })

  .state('app.are-you-an-expert', {
    url: '/are-you-an-expert',
    views: {
      'menuContent': {
        templateUrl: 'templates/are-you-an-expert.html',
        controller: 'AreYouAnExpertCtrl'
      }
    }
  })

  .state('app.contact', {
    url: '/contact',
    views: {
      'menuContent': {
        templateUrl: 'templates/contact.html',
        controller: 'ContactCrtl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});