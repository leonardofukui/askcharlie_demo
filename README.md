askCharlie DEMO Project
=====================

## Using this project

Make sure the `ionic` utility is installed:

```bash
$ npm install -g ionic
```

To run it on a web browser:

```bash
$ ionic serve
```

To run it on an Android device:
```bash
$ ionic platform run android
```

To run it on an iOS device: 
```bash
$ ionic platform run ios
```
